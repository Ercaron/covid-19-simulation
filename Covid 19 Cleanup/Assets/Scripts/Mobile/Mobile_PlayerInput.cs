﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mobile_PlayerInput : PlayerInput
{

    protected override void DetectInput()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (i >= 3) break;
            Touch touch = Input.GetTouch(i);
            if(touch.phase == TouchPhase.Began && touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                ExecuteInput(ray);
           
            }
        }
    }
}
