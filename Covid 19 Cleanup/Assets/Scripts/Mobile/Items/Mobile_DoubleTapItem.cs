﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mobile_DoubleTapItem : Item
{
    bool selected = false;
    GameObject target;
    [SerializeField] LayerMask layers;

    public GameObject Target { get => target; set => target = value; }

    public override void Use()
    {
        Debug.Log("debug.log");
        selected = true;

    }

    private void Update()
    {
        if (selected)
        {
            Debug.Log("debug.log2");
            int touches = Input.touchCount;
            if (touches > 0)
            {
                Touch touch = Input.touches[0];
                Ray ray = Camera.main.ScreenPointToRay(touch.position);

                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100, layers);
                Debug.Log("esperandooouuu");
                if (hit.collider)
                {

                    Person person = hit.collider.gameObject.GetComponent<Person>();
                    if (person)
                    {
                        Activate();
                        target = hit.collider.gameObject;
                    }
                    else
                    {
                        selected = false;
                        //desactivar particulas, glow

                    }
                }
            }
        }
    }
}
