﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespiratorItem : DoubleTapItem
{
    

    public override void Activate()
    {
        base.Activate();
        Person p = Target.GetComponent<Person>();
        MaskData maskData = Data as MaskData;
        p.InfectionThreshold += maskData.CoverAmount;
        GameObject go = Instantiate(maskData.Mask,p.MaskHolder.transform);
        go.transform.localPosition = new Vector2(0, 0);
    }
}
