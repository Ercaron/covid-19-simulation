﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaxineItem : DoubleTapItem
{
    

    public override void Activate()
    {
        base.Activate();
        Person p = Target.GetComponent<Person>();
        if (p.Infected) return;
        Target.GetComponent<BoxCollider2D>().enabled = false;
        
        
    }
}
