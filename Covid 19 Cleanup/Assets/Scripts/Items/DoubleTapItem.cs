﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleTapItem : Item
{
    bool selected = false;
    GameObject target;
    [SerializeField] LayerMask layers;

    public GameObject Target { get => target; set => target = value; }

    public override void Use()
    {
        base.Use();
        if(CanUse) selected = true;
    }

    public override void Activate()
    {
        base.Activate();
        selected = false;
    }

    private void Update()
    {
        
        if (selected) {
           if( Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100, layers);
                if (hit.collider)
                {

                    Person person = hit.collider.gameObject.GetComponent<Person>();
                    if (person)
                    {

                        target = hit.collider.gameObject;
                        Activate();

                    }
                    else
                    {
                        selected = false;
                        //desactivar particulas, glow

                    }
                }
            }
        }
    }
}
