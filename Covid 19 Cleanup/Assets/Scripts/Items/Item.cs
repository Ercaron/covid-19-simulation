﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    [SerializeField] ItemData data;
    [SerializeField] Image sprite;
    [SerializeField] Text itemCount;
    AudioSource audioSource;
    Player player;
    bool canUse;

    public ItemData Data { get => data; set => data = value; }
    public Player Player { get => player; set => player = value; }
    public bool CanUse { get => canUse; set => canUse = value; }


    private void Start()
    {
        if(sprite && data) sprite.sprite = Data.Icon;

        audioSource = GetComponent<AudioSource>();
        if (audioSource)
        {
            if(data.Sound) audioSource.clip = Data.Sound;
        }
        UpdateUI();
        

    }

    public virtual void Use()
    {
        if (player.Items[data.ItemIndex] <= 0)
        {
            CanUse = false;
        }
        else CanUse = true;
    }

    public virtual void Activate()
    {
        Player.Items[Data.ItemIndex] -= 1;
        if(audioSource && audioSource.clip) audioSource.Play();
        UpdateUI();
    }

    public virtual void Deactivate() { }


    public void UpdateUI()
    {
        if (Player.Items[Data.ItemIndex] <= 0)
        {
            sprite.color = new Color(171, 171, 171);
            Player.Items[Data.ItemIndex] = 0;
        }

        itemCount.text = player.Items[data.ItemIndex].ToString();
    }
}
