﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapItem : Item
{
    public override void Use()
    {
        base.Use();
        if (CanUse)
        {
            Activate();
            
        }
        
    }

    public override void Activate()
    {
        base.Activate();
    }
}
