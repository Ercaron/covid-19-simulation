﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/ItemData/Mask")]
public class MaskData : ItemData
{
    [SerializeField] GameObject mask;
    [SerializeField] float coverAmount;

    public GameObject Mask { get => mask; set => mask = value; }
    public float CoverAmount { get => coverAmount; set => coverAmount = value; }
}
