﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SO/ItemData/base")]
public class ItemData : ScriptableObject
{
    [SerializeField] Sprite icon;
    [SerializeField] string itemName;
    [SerializeField] string description;
    [SerializeField] int price;
    [SerializeField] float cooldown;
    [SerializeField] float duration;
    [SerializeField] int itemIndex;
    [SerializeField] AudioClip sound;

    public Sprite Icon { get => icon; set => icon = value; }
    public string Description { get => description; set => description = value; }
    public int Price { get => price; set => price = value; }
    public string ItemName { get => itemName; set => itemName = value; }
    public float Cooldown { get => cooldown; set => cooldown = value; }
    public float Duration { get => duration; set => duration = value; }
    public int ItemIndex { get => itemIndex; set => itemIndex = value; }
    public AudioClip Sound { get => sound; set => sound = value; }
}
