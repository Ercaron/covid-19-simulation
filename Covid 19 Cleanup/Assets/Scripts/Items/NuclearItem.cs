﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NuclearItem : TapItem
{

    [SerializeField]AudioSource sound;
    [SerializeField] Spawner spawner;
   

    public override void Activate()
    {
        base.Activate();

        for (int i = 0; i < spawner.SpawnedGermens.Count; i++)
        {
            spawner.SpawnedGermens[i].Damage(999999); //secure kill
            i--;
            if(sound) sound.PlayDelayed(4f);
        }
            //JUAN TODO: PARTICULA EXPLOSIVA CON SUS RELATIVOS SHADERS QUE NO CONSUMAN MUCHO EN UNA CAPACIDAD 3/4 DE MEMORIA
    }
}
