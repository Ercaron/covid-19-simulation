﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeItem : TapItem
{
   [SerializeField]Spawner spawn;
    List<Germen> germens;

    public override void Activate()
    {
        base.Activate();
        Invoke("Deactivate", Data.Duration);

        GameObject[] objects = GameObject.FindGameObjectsWithTag("Germen");

        germens = new List<Germen>();

        for (int i = 0; i < objects.Length; i++)
        {
            Germen ger = objects[i].GetComponent<Germen>();
            if (ger) germens.Add(ger);
        }
        for (int i = 0; i < germens.Count; i++)
        {
            germens[i].GrowRatio = 0;         
        }
        spawn.IsActive = false;

    }

    public override void Deactivate()
    {
        for (int i = 0; i < germens.Count; i++)
        {
            germens[i].GrowRatio = germens[i].Data.BaseGrowthRate;
        }
        spawn.IsActive = true;
    }

}
