﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hospital : MonoBehaviour
{
    List<Person> persons;
    [SerializeField] AudioSource src;
    float ratio = 2f;
    float degrees = 180;

    private void Start()
    {
        persons = new List<Person>();
    }

    private void Update()
    {
        if (persons.Count > 0) ReducePerson();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
    
        Person person = collision.GetComponent<Person>();
        
        if (person != null)
        {
            persons.Add(person);
        }
        src.Play();
    }

    void ReducePerson()
    {


        for (int i = 0; i < persons.Count; i++)
        {
            float scale = persons[i].transform.localScale.x;

            scale -= ratio * Time.deltaTime;

            persons[i].transform.localScale = new Vector2(scale, scale);
            persons[i].transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
            persons[i].transform.Rotate(0, 0, degrees * Time.deltaTime);

            

            if (persons[i].transform.localScale.x <= 0)
            {
                
                Destroy(persons[i].gameObject,0.05f);
                persons.Remove(persons[i]);
                i--;
            } 
        }
        
    }

}
