﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Levels/base")]
public class LevelData : ScriptableObject
{
    [SerializeField] float timeDuration;
    [SerializeField] float spawnRate;
    [SerializeField] List<Objective> objectives;
    [SerializeField] int index;
    [SerializeField] bool completed;

    public float TimeDuration { get => timeDuration; set => timeDuration = value; }
    public float SpawnRate { get => spawnRate; set => spawnRate = value; }
    public List<Objective> Objectives { get => objectives; set => objectives = value; }
    public int Index { get => index; set => index = value; }
    public bool Completed { get => completed; set => completed = value; }
}
