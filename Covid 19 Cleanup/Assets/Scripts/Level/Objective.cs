﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective : ScriptableObject
{
    [SerializeField] float amountNeeded;

    public float AmountNeeded { get => amountNeeded; set => amountNeeded = value; }
   

    public virtual bool CheckObjective(float amountToCheck)
    {
        return false; 
    }

}
