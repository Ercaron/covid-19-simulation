﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Objective/Persons Alive")]
public class Objective_PersonsAlive : Objective
{
    public override bool CheckObjective(float amountToCheck)
    {
        if (amountToCheck <= AmountNeeded) return true; //If i have 3 infected persons and i needed less or equal than 2 for this Objective, i get a false, so no objetiveCompleted
        else return false;
    }
}
