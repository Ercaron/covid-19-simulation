﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] LevelData data;
    [SerializeField] Spawner spawner;
    [SerializeField] GameManager game;
    [SerializeField] GameObject winLevelMenu;
    [SerializeField] GameObject loseLevelMenu;

    int obtainedStars = 0;
    int infectedPersons = 0;
    int personsCount;


    public LevelData Data { get => data; set => data = value; }
    public int ObtainedStars { get => obtainedStars; set => obtainedStars = value; }

    private void Start()
    {
        Invoke("Win", data.TimeDuration);
        spawner.SpawnRatio = data.SpawnRate;

        GameObject[] persons = GameObject.FindGameObjectsWithTag("Person");
        personsCount = persons.Length;
        for (int i = 0; i < persons.Length; i++)
        {
            Person p = persons[i].GetComponent<Person>();
            p.PlayerInfected += PlayerInfected;
        }

    }

    void FinishedLevel()
    {
        spawner.gameObject.SetActive(false);

        for (int i = 0; i < spawner.SpawnedGermens.Count; i++)
        {
            Destroy(spawner.SpawnedGermens[i].gameObject);
           
        }
        spawner.SpawnedGermens.Clear();

        

    }

    int CheckObtainedStars(LevelData level)
    {
        int completedObj = 0;
        for (int i = 0; i < level.Objectives.Count; i++)
        {
            if(level.Objectives[i].CheckObjective(infectedPersons)) completedObj++;
        }
        return completedObj;
    }

    void PlayerInfected()
    {
        infectedPersons++;

        if(infectedPersons == personsCount)
        {
            CancelInvoke("Win");
            Lose();

        }
    }


    void Lose()
    {
        FinishedLevel();
        if (loseLevelMenu) loseLevelMenu.SetActive(true);
    }

    void Win()
    {
        FinishedLevel();
        ObtainedStars = CheckObtainedStars(data);
        
       
        game.LevelFinished(this);
        data.Completed = true; //FIX Necesario? 

        if (winLevelMenu)
        {
            winLevelMenu.SetActive(true);
            WinMenu menu = winLevelMenu.GetComponentInChildren<WinMenu>();
            menu.ObtainedStars = ObtainedStars;
            menu.level = data.Index;
        }

    }
}
