﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfectedExpansion : MonoBehaviour
{
    float infectAmount = 200f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Person person = collision.GetComponentInParent<Person>();

        if (person)
        {
            person.Infect(infectAmount * Time.deltaTime);
        }
    }
}
