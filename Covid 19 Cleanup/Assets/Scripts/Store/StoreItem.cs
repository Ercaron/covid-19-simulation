﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreItem : MonoBehaviour
{

    //FIX PREFAB 

    //agregar count de cuanto tenes actualmente
    ItemData item;

    [SerializeField] Text numberOfItems;
    [SerializeField] AudioSource src;

    Button btn;
    Text[] textField;
    Store store;


    public ItemData Item { get => item; set { item = value; ItemDataChanged(); } }

    public Store Store { get => store; set => store = value; }
    public Text NumberOfItems { get => numberOfItems; set => numberOfItems = value; }

    void Awake()
    {
        btn = GetComponent<Button>();
        textField = GetComponentsInChildren<Text>();
      
    }

    
    void ItemDataChanged()
    {
        btn.image.sprite = item.Icon;
        textField[0].text = item.ItemName;
        textField[1].text = item.Description;
        textField[2].text = "$" + item.Price.ToString();
                
    }

    public void BuyItem()
    {
        store.BuyItem(item);
        src.Play();
    }

}
