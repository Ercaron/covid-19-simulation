﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Store : MonoBehaviour
{

    [SerializeField] Text coins;
    //[SerializeField] Text numberOfItems;
    [SerializeField] List<ItemData> items;
    [SerializeField] List<StoreItem> storeItems;


    Player player;
    GameData gameData;

    void Start()    
    {
        for (int i = 0; i < storeItems.Count; i++)
        {
            storeItems[i].Item = items[i];
            storeItems[i].Store = this;
            
        }

    }

    void Update()
    {
        coins.text = "Coins:"+ ((int)player.Coins).ToString();
        for (int i = 0; i < storeItems.Count; i++)
        {
            storeItems[i].NumberOfItems.text = player.Items[i].ToString();
        }
    }

    public void BuyItem(ItemData item)
    {
        for (int i = 0; i < player.Items.Length; i++)
        {
           
            Debug.Log(player.Items[i] + " Index " + i);
            
        }
       

        if (player.Coins >= item.Price)
        {
            player.Coins -= item.Price;
            player.Items[item.ItemIndex] += 1; 
            //actualizar contador de cuantos tengo
            Debug.Log("Pude comprar!");
        }
        else
        {
            Debug.Log("NAooo Pude comprar!"); return;
        }

    }



    private void OnEnable()
    {
        gameData = GamePersistence.LoadData();
        if(!player) player = GetComponent<Player>();
        player.LoadData(gameData);

    }

    private void OnDisable()//Add when level is completed. not getting disabled
    {
        GamePersistence.SaveData(player);
    }

}



