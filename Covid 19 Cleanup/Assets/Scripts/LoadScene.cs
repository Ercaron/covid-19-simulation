﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField] string sceneName;

    public string SceneName { get => sceneName; set => sceneName = value; }

    public void LoadNewScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    
    public void LoadNewScene(int index)
    {
        SceneManager.LoadScene(index);
    }
}
