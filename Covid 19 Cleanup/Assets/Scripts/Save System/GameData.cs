﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{
    public int Coins { get; set; }

    public int[] Items { get; set; }

    public int MaxLevelReached { get; set; }

    public int[] LevelsStars {get;set;}

    public int TotalStar
    {
        get{
            var total = 0;
            for (int i = 0; i < LevelsStars.Length; i++)
            {
                total += LevelsStars[i];
            }
            return total;
        } 
    }
}
