﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GamePersistence 
{

    public static void SaveData(Player player)
    {
        PlayerPrefs.SetInt("coins", player.Coins);

        PlayerPrefs.SetInt("itemsLength", player.Items.Length);
        for (int i = 0; i < player.Items.Length; i++)
        {
            PlayerPrefs.SetInt("item_"+i, player.Items[i]);
        }
        PlayerPrefs.SetInt("maxLevelReached", player.MaxLevelReached);

        PlayerPrefs.SetInt("starsLength", player.LevelsStars.Length);

        for (int i = 0; i < player.LevelsStars.Length; i++)
        {
            PlayerPrefs.SetInt("level_" + i, player.LevelsStars[i]);
        }
        

    }

    public static GameData LoadData()
    {
        int coins = PlayerPrefs.GetInt("coins");
        int itemsLength = PlayerPrefs.GetInt("itemsLength");
        if (itemsLength == 0) itemsLength = 4;
        int[] items = new int[itemsLength];
        for (int i = 0; i < itemsLength; i++)
        {
            items[i] = PlayerPrefs.GetInt("item_" + i);
        }
        int maxLevelReached = PlayerPrefs.GetInt("maxLevelReached");
        int starsLength = PlayerPrefs.GetInt("starsLength");

        if (starsLength == 0) starsLength = 10;
        int[] levelsStars = new int[starsLength];

        for (int i = 0; i < starsLength; i++)
        {
            levelsStars[i] = PlayerPrefs.GetInt("level_" + i);
        }
        

        GameData gameData = new GameData()
        {
            Coins = coins,
            Items = items,
            MaxLevelReached = maxLevelReached,
            LevelsStars = levelsStars,
        };

        return gameData;
    }



}
