﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField] LevelData level;
    [SerializeField] LevelData previous;


    Button btn;
    Text txt;

    void Start()
    {
        btn = GetComponent<Button>();
        txt = GetComponentInChildren<Text>();

        if (level) txt.text = "Level " + level.Index.ToString();

        if (!previous || (previous && previous.Completed)) btn.onClick.AddListener(LoadScene);

    }


    void LoadScene()
    {
        LoadScene loader = new LoadScene();
        loader.LoadNewScene(level.Index);
    }
   
}
