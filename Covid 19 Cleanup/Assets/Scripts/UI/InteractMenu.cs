﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractMenu : MonoBehaviour
{

    [SerializeField] List<GameObject> showWindows;
    [SerializeField] List<GameObject> hideWindows;

    AudioSource src;
    void Start()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(ChangeWindows);
       
    }

    void ChangeWindows()
    {
       

        for (int i = 0; i < showWindows.Count; i++)
        {
            showWindows[i].SetActive(true);
        }
        
        for (int i = 0; i < hideWindows.Count; i++)
        {
            hideWindows[i].SetActive(false);
        }
    }
}
