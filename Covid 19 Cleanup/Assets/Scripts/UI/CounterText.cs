﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterText : MonoBehaviour
{
    [SerializeField] Text text;
    [SerializeField] Level level;

    int time;

    private void Start()
    {
        time = (int)level.Data.TimeDuration;
        InvokeRepeating("Replace", 1,1);
    }

    void Replace()
    {
        time--;
        if(time == 0)
        {
            CancelInvoke();
            this.gameObject.SetActive(false);
        }
        text.text = time.ToString();
    }



}
