﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameObject pauseUI;

    public delegate void GamePaused(bool gameState);
    public event GamePaused e_GamePaused;

    
    bool paused = false;

    public void Resume()
    {
        pauseUI.SetActive(false);
        Time.timeScale = 1f;
        paused = false;
        e_GamePaused.Invoke(false);
    }

    public void Pause()
    {
        pauseUI.SetActive(true);
        Time.timeScale = 0f;
        paused = true;
        e_GamePaused.Invoke(true);
    }

}
