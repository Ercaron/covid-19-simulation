﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsUI : MonoBehaviour
{

    [SerializeField] List<Item> items;
    [SerializeField] GameManager gameManager;

    void Start()
    {
        
        for (int i = 0; i < items.Count; i++)
        {
            items[i].Player = gameManager.PlayerData;
        }
    }

}
