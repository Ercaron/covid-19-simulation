﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class WinMenu : MonoBehaviour
{
    [SerializeField] List<Image> stars;
    [SerializeField] TextMeshProUGUI levelText;
    [SerializeField] List<AudioClip> starsSounds;

    AudioSource src;
   [SerializeField] AudioSource srcFireworks;

    int index = 0;
    bool update;

    public int ObtainedStars { get; set; }
    public int level { get; set; }
   
    void Start()
    {
        src = GetComponent<AudioSource>();

        levelText.text = levelText.text.Replace("$", level.ToString());

        for (int i = 0; i < ObtainedStars; i++)
        {
            stars[i].gameObject.SetActive(true);
           
        }

        StartCoroutine("FillStar");
    }


 

    IEnumerator FillStar()
    {
        if (index >= ObtainedStars) yield break;
     
        while (stars[index].fillAmount != 1)
        {
            stars[index].fillAmount += 0.01f;
          
            yield return new WaitForSeconds(0.01f); 

        }
        src.clip = starsSounds[index];

        src.Play();
        if(index == 2)
        {
            
            srcFireworks.Play();
        }
        index++;
        StartCoroutine("FillStar");
    }

}
