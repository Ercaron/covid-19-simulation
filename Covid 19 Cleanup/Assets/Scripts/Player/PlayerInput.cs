﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] protected LayerMask layers;
    [SerializeField] protected float damage;

    protected Transform selectedPerson;
    protected bool hasSelection;
    private bool active = true;

    public bool Active { get => active; set => active = value; }

    void Update()
    {
        if (active)
        {
            DetectInput();
            if (hasSelection) MoveObject(selectedPerson);
        }
        
    }


    protected virtual void MoveObject(Transform obj)
    {
        obj.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }


    protected virtual void DetectInput()
    {
        if (Input.GetMouseButtonUp(0))
        {
            hasSelection = false;
            selectedPerson = null;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            Debug.DrawRay(ray.origin, ray.direction, Color.green, 5);
            ExecuteInput(ray);
            
        }
        
    }


    protected void ExecuteInput(Ray ray)
    {
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100, layers);

        if (hit.collider)
        {

            Germen ger = hit.collider.gameObject.GetComponent<Germen>();
            if (ger)
            {
                ger.Damage(damage);
            }
            else
            {
                Person person = hit.collider.gameObject.GetComponent<Person>();
                if (person && person.Infected)
                {
                    hasSelection = true;
                    selectedPerson = person.transform;
                }
            }

        }
    }
}
