﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Logic for the player actions will be handled in PlayerInput
    //The class Player is only used for holding the stats of the player

    int coins;
    int[] items;
    int maxLevelReached;
    int[] levelsStars;
    int totalStars;

    public int Coins { get => coins; set => coins = value; }
    public int[] Items { get => items; set => items = value; }
    public int MaxLevelReached { get => maxLevelReached; set => maxLevelReached = value; }
    public int[] LevelsStars { get => levelsStars; set => levelsStars = value; }
    public int TotalStars { get => totalStars; set => totalStars = value; }

    public void LoadData(GameData data)
    {
        coins = data.Coins;
        items = data.Items;
        maxLevelReached = data.MaxLevelReached;
        levelsStars = data.LevelsStars;
        totalStars = data.TotalStar;
    }
}
