﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/GermenData/base")]
public class GermenData : ScriptableObject
{
    [SerializeField] float baseGrowthRate;
    [SerializeField] int coins;
    [SerializeField] AudioClip sound;
    [SerializeField] GameObject particles;
    [SerializeField] Sprite sprite;

    public float BaseGrowthRate { get => baseGrowthRate; set => baseGrowthRate = value; }
    public int Coins { get => coins; set => coins = value; }
    public AudioClip Sound { get => sound; set => sound = value; }
    public GameObject Particles { get => particles; set => particles = value; }
    public Sprite Sprite { get => sprite; set => sprite = value; }
    
}
