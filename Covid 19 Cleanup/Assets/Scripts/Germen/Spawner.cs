﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameObject germen;
    [SerializeField] GameObject area;
    [SerializeField] float spawnRatio;
    [SerializeField] LayerMask blockedLayers; //So that germens are not spawned on those places (for example above persons)

    BoxCollider2D col;
    float minX,maxX,minY,maxY;
    float lastSpawn;
    bool isActive = true;
    List<Germen> spawnedGermens;

    public bool IsActive { get => isActive; set => isActive = value; }
    public List<Germen> SpawnedGermens { get => spawnedGermens; set => spawnedGermens = value; }
    public float SpawnRatio { get => spawnRatio; set => spawnRatio = value; }

    void Start()
    {
       
        col = area.GetComponent<BoxCollider2D>();

        minX = col.bounds.min[0];
        maxX = col.bounds.max[0];
        minY = col.bounds.min[1];
        maxY = col.bounds.max[1];

        spawnedGermens = new List<Germen>();
    }

   
    void Update()
    {
        if (IsActive)
        {
            if (Time.time >= SpawnRatio + lastSpawn)
            {
                Vector3 spawnPoint = GetRandomPos();
                while (true)
                {
                    if (IsValidSpawnPoint(spawnPoint)) break;
                    else spawnPoint = GetRandomPos();
                }
                GameObject go = Instantiate(germen, spawnPoint, transform.rotation);
                Germen ger = go.GetComponent<Germen>();
                spawnedGermens.Add(ger);
                ger.onDead += HandleGermenDeath;

                lastSpawn = Time.time;
               
            }
        }
    }

    

    Vector3 GetRandomPos()
    {
        float posX = Random.Range(minX, maxX);
        float posY = Random.Range(minY, maxY);
        
        return new Vector3(posX, posY, -1);
    }



    bool IsValidSpawnPoint(Vector3 pos)
    {
        Vector3 screenpoint = Camera.main.WorldToScreenPoint(pos);
        Ray ray = Camera.main.ScreenPointToRay(screenpoint);

        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction,20,blockedLayers);
        
        Debug.DrawRay(ray.origin, ray.direction*20,Color.yellow,2);
        

        if (hit.collider)
        {
            Person per = hit.collider.GetComponent<Person>();
            if (per) return false;
            else return true;
        }
        else return true;

      
    }


    void HandleGermenDeath(Germen germen)
    {
        germen.onDead -= HandleGermenDeath;
        gameManager.PlayerData.Coins += germen.Data.Coins;
        spawnedGermens.Remove(germen);
    }
}
