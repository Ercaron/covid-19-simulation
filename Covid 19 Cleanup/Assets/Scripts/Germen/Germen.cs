﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Germen : MonoBehaviour
{
    [SerializeField] AudioSource hitSound;
    [SerializeField] AudioSource dieSound;
    [SerializeField] GermenData data;
    [SerializeField] float health;
    [SerializeField] float infectAmount;
    [SerializeField] float maxGrowth = 0.4f;
    [SerializeField] GameObject particle;

    float scale;
    int coins;

    public float GrowRatio { get; set; }
    public GermenData Data { get => data; set => data = value; }
    public delegate void Dead(Germen germen);
   

    CircleCollider2D col;

    private void Awake()
    {
        scale = transform.localScale.x;
        col = GetComponent<CircleCollider2D>();
        GrowRatio = Data.BaseGrowthRate;
        coins = data.Coins;
    }


    void Update()
    {
        scale += GrowRatio * Time.deltaTime;
        health = scale;
        transform.localScale = new Vector3(scale, scale);
        ScaleLimit();
    }

    public void Damage(float damage)
    {
        hitSound.Play();
        health -= damage;
        scale -= damage;
        transform.localScale = new Vector3(scale, scale);

        if (health <= 0.1f)
        {
            dieSound.gameObject.SetActive(true);
            dieSound.Play();

            particle.SetActive(true);

            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            onDead.Invoke(this);          
            Destroy(gameObject,0.4f);
        }
        
    }

    void ScaleLimit()
    {
        if (scale >= maxGrowth)
        {
            scale = maxGrowth;
        }
    }

    public event Dead onDead;

    private void OnTriggerStay2D(Collider2D collision)
    {
        Person person = collision.attachedRigidbody.gameObject.GetComponent<Person>();

        if (person)
        {
            person.Infect(infectAmount * Time.deltaTime);
        }
    }


}
