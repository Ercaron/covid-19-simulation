﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Person : MonoBehaviour
{
    public delegate void InfectedD();
    public event InfectedD PlayerInfected;

    [SerializeField] GameObject maskHolder;
    [SerializeField] GameObject infectionRadius;
    [SerializeField] Image img;
    float infectionThreshold = 100;
    bool infected;
    float infection;
    float infectionRatio = 0.08f;
    float scale;

    public float Infection { get => infection; set { infection = value; InfectionChanged(); } }

    public bool Infected { get => infected; set => infected = value; }
    public float InfectionThreshold { get => infectionThreshold; set => infectionThreshold = value; }
    public GameObject MaskHolder { get => maskHolder; set => maskHolder = value; }

    public void Infect(float infectAmount)
    {
        Infection += infectAmount;
    }

    void InfectionChanged()
    {
        img.fillAmount = infection/InfectionThreshold;


      if (!infected && infection >= InfectionThreshold)
        {
            
            infected = true;
            infectionRadius.SetActive(true);
            
            PlayerInfected.Invoke(); 
        }
    }

    private void Update()
    {
        if (infected)
        {
            scale = infectionRadius.transform.localScale.x;
            scale += infectionRatio * Time.deltaTime;
            infectionRadius.transform.localScale = new Vector3(scale, scale);
        }
    }
}
