﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] PlayerInput playerInput;
    [SerializeField] PauseMenu pauseMenu;
    [SerializeField] AudioSource src;

    bool gamePaused;
    Player playerData;



    public GameData GameData { get; private set; }

    public bool GamePaused { get => gamePaused; set { gamePaused = value; playerInput.Active = !gamePaused; } }

    public Player PlayerData { get => playerData; set => playerData = value; }


    void Start()
    {
        pauseMenu.e_GamePaused += SetGameState;
        src.Play();
        GameData = GamePersistence.LoadData();
        PlayerData = GetComponent<Player>();
        PlayerData.LoadData(GameData);
    }

    public void SetGameState(bool gameState)
    {
        GamePaused = gameState;
    }

    private void OnDisable()//Add when level is completed. not getting disabled
    {
        GamePersistence.SaveData(PlayerData);
    }


    public void LevelFinished(Level level)
    {
        if (PlayerData.MaxLevelReached < level.Data.Index)
        {
            PlayerData.MaxLevelReached = level.Data.Index;
        }

        if(PlayerData.LevelsStars[level.Data.Index-1] < level.ObtainedStars)
        {
            PlayerData.LevelsStars[level.Data.Index - 1] = level.ObtainedStars;
            
        }
    }
}
