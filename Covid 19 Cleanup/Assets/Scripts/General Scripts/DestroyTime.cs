﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTime : MonoBehaviour
{
    ParticleSystem ps;

    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();
        Invoke("DestroyInTime", ps.main.duration);
    }

    void DestroyInTime()
    {
        Destroy(this);
    }

}
